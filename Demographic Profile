-- Demographic Report


--- Gender

SELECT geo, 
       COUNT(DISTINCT estid) AS ShareThis_Count
  FROM (
    SELECT *
      FROM (
        SELECT d.estid, d.geo,
        IF(is_nan(domain_male),null,domain_male) AS male,
        ROW_NUMBER() OVER(PARTITION BY d.estid ORDER BY date(date_parse(dt, '%Y%m%d')) DESC) AS rownumber
        FROM (
      	  SELECT estid, geo
          FROM sharethis.piracy_domains pd
          INNER JOIN market_analytics_prod_latest.sites_of_interest_single_row_per_site soi ON (soi.feedid = pd.feedid)
          WHERE geo = 'gb'
          AND sitetypes IN ('TV', 'FILM', 'FILM,TV', 'FILM/TV', 'TV,FILM')
          AND regexp_like(concat(url, '?', url_metadata_raw_query_params), 'black.{0,2}widow')
          AND date(day) >= date('2021-01-01')
          GROUP BY estid, geo
        ) a
        JOIN (SELECT * FROM sharethis.demographic WHERE geo = 'gb') d ON (d.estid = a.estid AND d.geo = a.geo)
      ) AS t
WHERE t.rownumber = 1 AND male <> 0.5)
GROUP BY geo
ORDER BY geo


-- Age

SELECT
    geo,
    AVG(age_18_24) AS age18_24,
    AVG(age_25_34) AS age25_34,
    AVG(age_35_44) AS age35_44,
    AVG(age_45_54) AS age45_54,
    AVG(age_55_64) AS age55_64,
    AVG(age_65) AS age65,
    COUNT(DISTINCT estid) AS ShareThis_Count
FROM (
  SELECT *
    FROM (
      SELECT d.estid, d.geo,
         IF(is_nan(f_domain_age_18_24),null,f_domain_age_18_24) AS age_18_24,
         IF(is_nan(f_domain_age_25_34),null,f_domain_age_25_34) AS age_25_34,
         IF(is_nan(f_domain_age_35_44),null,f_domain_age_35_44) AS age_35_44,
         IF(is_nan(f_domain_age_45_54),null,f_domain_age_45_54) AS age_45_54,
         IF(is_nan(f_domain_age_55_64),null,f_domain_age_55_64) AS age_55_64,
         IF(is_nan(f_domain_age_65_plus),null,f_domain_age_65_plus) AS age_65,  
      ROW_NUMBER() OVER(PARTITION BY d.estid ORDER BY  date(date_parse(dt, '%Y%m%d')) DESC ) AS rownumber
      FROM (
      	  SELECT estid, geo
		  FROM sharethis.piracy_domains pd
		  INNER JOIN market_analytics_prod_latest.sites_of_interest_single_row_per_site soi ON (soi.feedid = pd.feedid)
		  WHERE geo = 'gb'
		  AND sitetypes IN ('TV', 'FILM', 'FILM,TV', 'FILM/TV', 'TV,FILM') 
          AND regexp_like(concat(url, '?', url_metadata_raw_query_params), 'black.{0,2}widow')
		  AND date(day) >= date'2021-07-01'
		  AND lotame_id IS NOT NULL
		  AND lotame_id != '0'
		  AND lotame_id NOT LIKE '%-%'
		  AND lotame_id NOT LIKE '%profile%'
		  GROUP BY estid, geo
      ) a
      JOIN (SELECT * FROM sharethis.demographic WHERE geo = 'gb') d 
      ON (d.estid = a.estid AND d.geo = a.geo)
    ) AS t
    WHERE t.rownumber = 1 
    AND NOT (age_18_24 = age_25_34
			AND age_25_34 = age_35_44
			AND age_35_44 = age_45_54
			AND age_45_54 = age_55_64
			AND age_55_64 = age_65)
) 
GROUP BY geo
ORDER BY geo



-- income

SELECT geo,
    AVG(income_0_30) AS income0_30,
    AVG(income_30_60) AS income30_60,
    AVG(income_60_100) AS income60_100,
    AVG(income_100) AS income100,
    COUNT(DISTINCT estid) AS ShareThis_Count
FROM (
  SELECT *
    FROM (
      SELECT d.estid, d.geo,
         IF(is_nan(f_domain_income_0_to_30k),null,f_domain_income_0_to_30k) AS income_0_30,
         IF(is_nan(f_domain_income_30_to_60k),null,f_domain_income_30_to_60k) AS income_30_60,
         IF(is_nan(f_domain_income_60_to_100k),null,f_domain_income_60_to_100k) AS income_60_100,
         IF(is_nan(f_domain_income_100k_plus),null,f_domain_income_100k_plus) AS income_100,
      ROW_NUMBER() OVER(PARTITION BY d.estid ORDER BY  date(date_parse(dt, '%Y%m%d')) DESC ) AS rownumber
      FROM (
      	  SELECT estid, geo
		  FROM sharethis.piracy_domains pd
		  INNER JOIN market_analytics_prod_latest.sites_of_interest_single_row_per_site soi ON (soi.feedid = pd.feedid)
		  WHERE geo = 'gb'
		  AND sitetypes IN ('TV', 'FILM', 'FILM,TV', 'FILM/TV', 'TV,FILM') 
          AND regexp_like(concat(url, '?', url_metadata_raw_query_params), 'black.{0,2}widow')
		  AND date(day) >= date'2021-07-01'
		  GROUP BY estid, geo
      ) a
      JOIN (SELECT * FROM sharethis.demographic WHERE geo = 'gb') d 
      ON (d.estid = a.estid AND d.geo = a.geo)
    ) AS t
    WHERE t.rownumber = 1 
    AND NOT (income_0_30 = income_30_60 AND income_30_60 = income_60_100 AND income_60_100 = income_100)
)
GROUP BY geo
ORDER BY geo


-- education

SELECT
    geo,
    AVG(education_no_college) AS no_college,
    AVG(education_some_college) AS some_college,
    AVG(education_graduate_school) AS graduate_college,
    AVG(education_college) AS college,
    COUNT(DISTINCT estid) AS ShareThis_Count
FROM (
  SELECT *
    FROM (
      SELECT d.estid, d.geo,
         IF(is_nan(f_domain_education_no_college),null,f_domain_education_no_college) AS education_no_college,
         IF(is_nan(f_domain_education_some_college),NULL,f_domain_education_some_college) AS education_some_college,
         IF(is_nan(f_domain_education_graduate_school),NULL,f_domain_education_graduate_school) AS education_graduate_school,
         IF(is_nan(f_domain_education_college),NULL,f_domain_education_college) AS education_college,
      ROW_NUMBER() OVER(PARTITION BY d.estid ORDER BY  date(date_parse(dt, '%Y%m%d')) DESC ) AS rownumber
      FROM (
      	  SELECT estid, geo
		  FROM sharethis.piracy_domains pd
		  INNER JOIN market_analytics_prod_latest.sites_of_interest_single_row_per_site soi ON (soi.feedid = pd.feedid)
		  WHERE geo = 'gb'
		  AND sitetypes IN ('TV', 'FILM', 'FILM,TV', 'FILM/TV', 'TV,FILM') 
          AND regexp_like(concat(url, '?', url_metadata_raw_query_params), 'black.{0,2}widow')
		  AND date(day) >= date'2021-07-01'
		  GROUP BY estid, geo
      ) a
      JOIN (SELECT * FROM sharethis.demographic WHERE geo = 'gb') d 
      ON (d.estid = a.estid AND d.geo = a.geo)
    ) AS t
    WHERE t.rownumber = 1 
    AND NOT (education_no_college = education_some_college AND education_some_college = education_graduate_school AND education_graduate_school = education_college)
)
GROUP BY geo
ORDER BY geo